/*__________ Full Page +++ __________*/
$(document).ready(function () {

	var fpElements = $('.fp');


	try {
		$('.fullpage').fullpage({
			
			/*____ Navigation ____*/
			menu: '.pg_nav_menu', // Menu bilan anchorni bog`lash
			lockAnchors: false, // URL da anchor yozuvini chiqarmaslik
			// anchors:['firstPage', 'secondPage'], // Section larga JS orqali anchor berish (Unique bo`lishi kerak va soni to`g`ri kelishi kerak)
			slidesNavigation: true, // Slide circle larini ko`rsatish
			slidesNavPosition: 'bottom', // Slide circle larini joyini aniqlash


			/*____ Scrolling ____*/
			css3: true, // Scroll uchun css3 dan foydalanish
			scrollingSpeed: 1000, // Scroll ni tezligini aniqlash
			autoScrolling: true, // Smooth scroll effect qo`shish
			scrollBar: false, // Scrollbar qo`shish
			easing: 'easeInOutCirc', // Scroll uchun JQuery timing-function dan foydalanish
			easingcss3: 'cubic-bezier(0.7, 0, 0.3, 1)', // Scroll uchun css3 timing-function dan foydalanish
			loopHorizontal: true, // Slide larni takrorlash
			normalScrollElements: '.tgr_mdl_bx, .tgr_mdl_bg', // Ustida scroll qilinsa scroll bo`lmaydigan element lar


			/*____ Accessibility ____*/
			keyboardScrolling: true, // Klaviatura orqali scroll qilish
			animateAnchor: true, // Agar site load bo`lsa, User kelgan joyiga animation bilan tushirish


			/*____ Design ____*/
			controlArrows: true, // Slide dagi arrow larni ko`rsatish
			verticalCentered: false, // Section ga '.fp-table' class qo`shish va vertical o`q bo`yicha o`rtaga joylashtirish
			// fixedElements: '#header, .footer', // Element larni Section ni tashqarisiga chiqarib tashlash
			responsiveWidth: 992, // Eni berilgan qiymatga yetsa, responsive mode ga o`tkazish yoki 'autoScrolling: false' qilish
			responsiveHeight: 550, // Bo`yi berilgan qiymatga yetsa, responsive mode ga o`tkazish yoki 'autoScrolling: false' qilish

			lazyLoading: true, // Rasmlarni Section ochilganidan keyin yuklash


			/*____ Events ____*/
			afterRender: function(){
				setInterval(function () {
					$.fn.fullpage.moveSlideRight();
				}, 7000);
			},
			afterResponsive: function(isResponsive){
				if (isResponsive)
				{
					fpElements.removeClass('fp');
					console.log('aaa');
				}
				else
				{
					fpElements.addClass('fp');
					console.log('bbb');
				}
			}
		});
	}
	catch (e) {
		console.warn('FullPage cannot find elements');
	}
});
/*__________ Full Page --- __________*/




/*__________ WOW JS +++ __________*/
try {
	new WOW().init();
}
catch (e) {
	console.warn("WOW JS cannot find elements");
}
/*__________ WOW JS --- __________*/






/*__________ Mobile Menu +++ __________*/
$(document).on('click', '.main_nav_menu_btn, .main_nav_menu_bg', function () {
	$('.main_nav_menu_btn').toggleClass('active');
	$('.main_nav_menu').toggleClass('active');
	$('.main_nav_menu_bg').toggleClass('active');
	$('html').toggleClass('ov-h');
});
/*__________ Mobile Menu --- __________*/






/*__________ Partners +++ __________*/
$(document).ready(function () {
	
	try {
		var owl = $('.isu_ptnr_bnr');
		
		owl.owlCarousel({
			items: 5,
			lazyLoad: true,
			navigation: false,
			pagination: false
		});
		
		
		// Custom Navigation Events
		$(".isu_ptnr_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});
		$(".isu_ptnr_bnr_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn('Owl Carousel cannot find ".isu_ptnr_bnr" element');
	}
	
});
/*__________ Partners --- __________*/





/*__________ Work Together +++ __________*/
$(document).on('click', '.work_advg_tg_txt > a, .tgr_mdl_bg, .tgr_mdl_close > button', function () {
	$('.tgr_mdl_bx').toggleClass('active');
	$('.tgr_mdl_bg').toggleClass('active');
	$('html').toggleClass('ov-h');
	
	return false;
});
/*__________ Work Together --- __________*/







/*__________ Breadcrumb +++ __________*/
$(document).on('click', '.hdr_bread_ls > li:not(.home) > a', function () {
	$(this).parent().toggleClass('active').siblings().removeClass('active');
	
	return false;
});

$(document).click(function (e) {
	if ($(e.target).is('.hdr_bread_ls, .hdr_bread_ls *') === false) {
		$('.hdr_bread_ls > li').removeClass('active');
	}
});
/*__________ Breadcrumb --- __________*/






/*__________ Importent Notice +++ __________*/
$(document).on('click', '.ntc_tb_ttl', function () {
	$(this).closest('.tr_bx').addClass('active');
});


$(document).on('click', '.ntc_tb_drp_cls > .txt', function () {
	$(this).closest('.tr_bx').removeClass('active');
});
/*__________ Importent Notice --- __________*/






/*__________ Row Grid +++ __________*/
$(document).ready(function () {
	
	try {
		$(".div_gal").rowGrid({
			itemSelector: ".div_gal_it",
			minMargin: 10,
			maxMargin: 10,
			firstItemClass: "first-item",
			lastRowClass: 'last-row',
			resize: true
		});
	}
	catch (e) {
		console.warn('Row Grid cannot find elements');
	}
	
});
/*__________ Row Grid --- __________*/





/*__________ Slick Slider +++ __________*/
try {
	$('.prd_bnr').slick({
		infinite: false,
		arrows: false,
		dots: false,
		asNavFor: '.prd_bnr_nav'
	});
	$('.prd_bnr_nav').slick({
		infinite: false,
		asNavFor: '.prd_bnr',
		// variableWidth: true,
		// centerMode: true,
		// slidesToShow: 5,
		// slidesToScroll: 5,
		swipeToSlide: true,
		focusOnSelect: true,
		prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left"></i></div>',
		nextArrow: '<div class="slick-next"><i class="fa fa-angle-right"></i></div>',
		responsive: [
			{
				breakpoint: 2000,
				settings: {
					slidesToShow: 8
				}
			},
			{
				breakpoint: 1590,
				settings: {
					slidesToShow: 6
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});
}
catch (e) {
	console.warn('Slick Slider cannot find elements');
}
/*__________ Slick Slider --- __________*/



























/*__________ Validation +++ __________*/


